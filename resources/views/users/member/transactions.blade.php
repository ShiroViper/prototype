@extends('layouts.app')

@section('title')
<title>Alkansya</title>
@endsection

@section('content')
<h3>Transactions</h3>
<div class="table-responsive pt-3">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Account</th>
                <th>Date</th>
                <th>Type</th>
                <th>Collector</th>
                <th>Amount</th>
                <th>Balance</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
            </tr>  
            <tr>
                <td>2</td>
                <td>2</td>
                <td>2</td>
                <td>2</td>
                <td>2</td>
                <td>2</td>
            </tr>  
        </tbody>
    </table>
</div>
@endsection
