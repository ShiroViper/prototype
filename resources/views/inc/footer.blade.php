<footer class="border-top border-bottom p-3 mt-3 card-footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="clearfix">
                   <small> © 2018 Alkansya App</small>
                    <div class="float-right">
                        <small><a href="/terms">Terms and Conditions</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
